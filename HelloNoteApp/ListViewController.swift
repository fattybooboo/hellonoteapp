//
//  ViewController.swift
//  HelloNoteApp
//
//  Created by 劉家維 on 2018/7/24.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit


class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var data : [Note] = []
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadFromFile()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - edit.
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
    }
    @IBAction func editBtn(_ sender: UIBarButtonItem) {
        self.tableView.setEditing(!self.tableView.isEditing, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.data.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        self.saveToFile()
    }
    
    
    //MARK: - add.
    @IBAction func addBtn(_ sender: UIBarButtonItem) {
        let note = Note()
        note.text = ""
        self.data.insert(note, at: 0)
        let indexPaths = IndexPath(row: 0, section: 0)
        self.tableView.insertRows(at: [indexPaths], with: .automatic)
        
        self.saveToFile()
    }
    
    
    
    
/////////////////////////////////////
}

extension ListViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - UITableViewDataSource methods.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        let note = data[indexPath.row]
        cell.noteField.text = note.text
        
       
        let now = Date()
        cell.dateLabel.text = DateFormatter.localizedString(from: now, dateStyle: .short, timeStyle: .none)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    
}

extension ListViewController: CustomTableViewCellDelegate {
    //MARK: - Prepare Archiving.
    func saveToFile() {
        let homeUrl = URL(fileURLWithPath: NSHomeDirectory()) //String to URL.
        let docUrl = homeUrl.appendingPathComponent("Documents")
        let fileUrl = docUrl.appendingPathComponent("notes.txt")
        
        NSKeyedArchiver.archiveRootObject(self.data, toFile: fileUrl.path)
//        print("home = \(fileUrl.path)")
    }
    
    func loadFromFile() {
        let homeUrl = URL(fileURLWithPath: NSHomeDirectory()) //String to URL.
        let docUrl = homeUrl.appendingPathComponent("Documents")
        let fileUrl = docUrl.appendingPathComponent("notes.txt")
        do {
            
            let data = try Data(contentsOf: fileUrl) //URL to Data.
            if let array = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Note] {
                self.data = array
            }else{
                self.data = []
            }
            
        }catch{
            print("error\(error)")
        }
        
        
    }
    
}
