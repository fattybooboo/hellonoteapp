//
//  CustomTableViewCell.swift
//  HelloNoteApp
//
//  Created by 劉家維 on 2018/7/24.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

protocol CustomTableViewCellDelegate:class {
    func saveToFile()
}


class CustomTableViewCell: UITableViewCell {

    var delegate : ListViewController?
    @IBOutlet weak var theSwitch: UISwitch!
    @IBOutlet weak var noteField: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.noteField.delegate=self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: switch.
    @IBAction func onOffSwitch(_ sender: UISwitch) {
        let note = noteField.text
        if theSwitch.isOn {
//            print("switch is on.")
            if note != nil {
                noteField.attributedText = NSAttributedString(string: "\(note!)", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red, NSAttributedStringKey.strikethroughStyle: 2, NSAttributedStringKey.strikethroughColor:UIColor.red])
                self.delegate?.saveToFile()
            }
        }
        if !theSwitch.isOn {
//            print("switch is off.")
            if note != nil {
                noteField.attributedText = NSAttributedString(string: "\(note!)", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
                self.delegate?.saveToFile()
            }
        }
    }
    
    
    
///////////////////////////////////////
}

extension CustomTableViewCell : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        noteField.resignFirstResponder()
        return true
    }
    
}
