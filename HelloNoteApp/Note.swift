//
//  Note.swift
//  HelloNoteApp
//
//  Created by 劉家維 on 2018/7/24.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import Foundation

class Note:NSObject, NSCoding {
    
    //Note to File.
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.noteID, forKey: "noteID")
        aCoder.encode(self.text, forKey: "text")
    }
    
    //File to Note.
    required init?(coder aDecoder: NSCoder) {
        self.noteID = aDecoder.decodeObject(forKey: "noteID") as! String
        self.text = aDecoder.decodeObject(forKey: "text") as? String
    }
    
    
    var text : String?
    var noteID : String
    override init() {
        noteID = UUID().uuidString
        super.init()
    }
}
